#!/bin/bash
response=$(curl -s -w "%{http_code}" http://127.0.0.1:8888)
http_code=$(tail -n1 <<< "$response")  # get the last line
echo $http_code
if [ $http_code = 200 ]
then
exit 0
else
exit 1
fi
