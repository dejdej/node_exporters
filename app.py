#!/usr/bin/env python
""" prometheus exporter """
from wsgiref.simple_server import make_server
import os
import psutil
from prometheus_client import Info, make_wsgi_app
from prometheus_client.core import GaugeMetricFamily, REGISTRY

class CustomCollector():
    """ <metric name>{<label name>=<label value>, ...} """
    def __init__(self):
        pass
    @classmethod
    def collect(cls):
        """
        pmem(rss=12914688, vms=23855104, shared=6848512 ...)
        """
        g_metric = GaugeMetricFamily("MemoryUsage", 'RSS memory usage [bytes]', labels=['instance'])
        g_metric.add_metric([os.uname()], psutil.Process(os.getpid()).memory_info().rss)
        yield g_metric


if __name__ == "__main__":

    REGISTRY.register(CustomCollector())

    ## metric types
    # counter = prom.Counter('python_my_counter', 'This is my counter')
    # gauge = prom.Gauge('python_my_gauge', 'This is my gauge')
    # histogram = prom.Histogram('python_my_histogram', 'This is my histogram')
    # summary = prom.Summary('python_my_summary', 'This is my summary')

    ## single time series
    i = Info('my_build_version', 'build info')
    i.info({'version': '8.0.0', 'buildhost': os.name })
    ## WSGI
    app = make_wsgi_app()
    httpd = make_server('', 8888, app)
    httpd.serve_forever()
