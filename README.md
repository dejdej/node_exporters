# node_exporters

* python node exporter:

    - start exporter `python app.py` exposed metrics @ `http://localhost:8000/metrics`
    - start exporter as container:
     ```bash 
    # from repos private registry
     docker login registry.gitlab.com 

    # from dockerhub
     docker run -d -p 8080:8888 dejanualex/python_exporter:1.0`
    ```
    - libs documentation: https://github.com/prometheus/client_python
    
