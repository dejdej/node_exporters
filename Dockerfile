FROM python:3.6

ADD . /python_exporter
RUN pip install -r /python_exporter/requirements.txt

WORKDIR /python_exporter
ENV PYTHONPATH '/python_exporter/'

CMD ["python" , "/python_exporter/app.py"]
